<?php

/**
 * Inter, http://tiraspol.me/, 30.10.2012;
 */

if (!defined('SMF'))
	die('Hacking attempt...');

global $context;

# Настройки
$context['limit_posts'] = array(
	# Bool - TRUE или 1 - Включить мод, FALSE или 0 - выключить мод
	'enable' => TRUE,

	# Int - Max количество тем за сутки
	'total' => 3,

	# Bool - Проверка по времени: TRUE или 1 - включить, FALSE или 0 - выключить
	'check_time' => TRUE,

	# Int - Max время проверки(секунды)
	'time' => 86400,

	# Array - ID форумов, в которых будет включён мод
	'id_boards' => array(45),

	# Bool
	# TRUE или 1 - Искать кол-во созданных тем во всех перечисленных форумах
	# FALSE или 0 - Искать кол-во созданных тем в конкретном текущем форуме (рекомендуется)
	'find_all_boards' => FALSE,

	# Array - ID групп пользователей, которые не будут проверяться модом - т.е. без ограничений
	'allowed_id_groups' => array()
);

# Сообщение об ошибке
$context['limit_posts']['error_msg'] = 'Вам запрещено создавать больше ' . $context['limit_posts']['total'] . ' тем в сутки в этом разделе!';

# return bool
function checkLimitPosts()
{
	global $smcFunc, $context, $board, $topic, $user_info, $modSettings, $txt;

	if ($user_info['is_admin']) # || $user_info['is_mod'])
		return TRUE;

	if (!$context['limit_posts']['enable'])
		return TRUE;

	if (!(empty($_REQUEST['msg']) && empty($topic)))
		return TRUE;

	if (empty($context['limit_posts']['id_boards']) or !is_array($context['limit_posts']['id_boards']))
		return TRUE;

	if (array_search($board, $context['limit_posts']['id_boards']) === FALSE)
		return TRUE;

	if (!empty($context['limit_posts']['allowed_id_groups']) and count(array_diff($user_info['groups'], $context['limit_posts']['allowed_id_groups'])) == 0)
		return TRUE;
	else
	{
		$request = $smcFunc['db_query']('', '
			SELECT COUNT(*)
			FROM {db_prefix}topics AS tpc
				INNER JOIN {db_prefix}messages AS msg ON (msg.id_msg = tpc.id_first_msg)
			WHERE tpc.id_member_started = {int:id_member}' . (!$context['limit_posts']['check_time'] ? '' : '
				AND msg.poster_time >= msg.poster_time - {int:poster_time}') . '
				AND tpc.id_board' . ($context['limit_posts']['find_all_boards'] ? ' IN ({array_int:id_board})' : ' = {int:id_board}') . (!$modSettings['postmod_active'] || allowedTo('approve_posts') ? '' : '
				AND tpc.approved = {int:approved}') . '
			LIMIT 1',
			array(
				'id_member' => $user_info['id'],
				'poster_time' => $context['limit_posts']['time'],
				'id_board' => $context['limit_posts']['find_all_boards'] ? $context['limit_posts']['id_boards'] : $board,
				'approved' => 1
			)
		);
		if ($smcFunc['db_num_rows']($request) == 0)
			fatal_error('Ошибка базы данных. Сообщите администратору.', FALSE);
		list ($total) = $smcFunc['db_fetch_row']($request);
		$smcFunc['db_free_result']($request);

		if ($total < $context['limit_posts']['total'])
			return TRUE;
		else
		{
			# $context['post_error']['limit_posts'] 
			$txt['error_limit_posts'] = $context['limit_posts']['error_msg'];
			return FALSE;
		}
	}

	trigger_error('Unknown Error.', E_USER_WARNING);
}